import axios from 'axios';

import { API_URL } from '@/app/constants';

export const apiClient = axios.create({ baseURL: API_URL });
