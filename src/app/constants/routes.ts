export const AUTH_ROUTE = '/auth';
export const REGISTER_STUDENT_ROUTE = '/registration/student';
export const REGISTER_TEACHER_ROUTE = '/registration/teacher';

export const COURSES_ROUTE = '/courses';

export const PROBLEM_ROUTE = '/problems';

export const USERS_ROUTE = '/users';
