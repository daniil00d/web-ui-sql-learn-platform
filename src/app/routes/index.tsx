import { lazy } from 'react';
import { AUTH_ROUTE, REGISTER_STUDENT_ROUTE } from '@/app/constants/routes';
import { NotFoundError } from '@/pages';
import { Route } from '@/shared/types';

const AuthPage = lazy(() => import('@/pages/auth'));
const RegisterPage = lazy(() => import('@/pages/registration'));
const LogoutPage = lazy(() => import('@/pages/logout'));
const RootPage = lazy(() => import('@/pages/Root'));
const UserRatingPage = lazy(() => import('@/pages/userRating'));
const CourseListPage = lazy(() => import('@/pages/courses/list'));
const CourseDetailPage = lazy(() => import('@/pages/courses/detail'));
const LessonDetailPage = lazy(() => import('@/pages/lessons/detail'));
const TaskDetailPage = lazy(() => import('@/pages/tasks/detail'));
const TaskListPage = lazy(() => import('@/pages/tasks/list'));

export const routes: Route[] = [
  {
    path: '/logout',
    element: <LogoutPage />,
    exact: true,
    redirect: '/',
  },
  {
    path: REGISTER_STUDENT_ROUTE,
    element: <RegisterPage />,
    exact: true,
  },
  {
    path: AUTH_ROUTE,
    element: <AuthPage />,
    exact: true,
  },
  {
    path: '/tasks',
    element: <TaskListPage />,
    exact: true,
  },
  {
    path: '/tasks/:id',
    element: <TaskDetailPage />,
    exact: true,
  },
  {
    path: '/lessons/:id',
    element: <LessonDetailPage />,
    exact: true,
  },
  {
    path: '/users/rating',
    element: <UserRatingPage />,
    exact: true,
  },
  {
    path: '/courses',
    element: <CourseListPage />,
    exact: true,
  },
  {
    path: '/courses/:id',
    element: <CourseDetailPage />,
    exact: true,
  },
  {
    path: '/',
    element: <RootPage />,
    exact: true,
  },
  {
    path: '*',
    element: <NotFoundError />,
  },
];
