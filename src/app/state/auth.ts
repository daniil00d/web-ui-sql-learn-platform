import { makeAutoObservable } from 'mobx';
import {
  DecodedAccessToken,
  isAuth as getAuthFlag,
  getDecodedAccessToken,
} from '@/shared/utils/tokens';

export class AuthStore {
  private token = localStorage.getItem('accessToken');
  public isAuth = getAuthFlag();
  public clientClaims: DecodedAccessToken | null = getDecodedAccessToken(this.token);

  constructor() {
    makeAutoObservable(this);
  }

  login(token: string) {
    this.token = token;
    this.clientClaims = getDecodedAccessToken(token);

    this.isAuth = true;
  }

  logout() {
    this.isAuth = false;
    this.token = '';
    this.clientClaims = null;
    localStorage.removeItem('accessToken');
  }
}

export const authStore = new AuthStore();
