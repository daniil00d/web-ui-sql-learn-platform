import { routes } from '@/app/routes';
import { Header, Loader } from '@/shared/components';
import { authRedirect } from '@/shared/hocs/authRedirect';
import { Suspense } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { authStore } from '@/app/state';
import CssBaseline from '@mui/material/CssBaseline';

const MainLayoutComp = () => {
  const isAuth = authStore.isAuth;
  console.log({ isAuth });

  return (
    <div className='main'>
      <CssBaseline />
      <Header />
      <Suspense fallback={<Loader />}>
        <Routes>
          {routes.map((route) => {
            if (route.redirect)
              return (
                <Route
                  path={route.path}
                  element={
                    <>
                      {route.element}
                      <Navigate to={route.redirect} replace />
                    </>
                  }
                />
              );

            const routeElement = <Route path={route.path} element={route.element} />;

            return route.private ? authRedirect(routeElement) : routeElement;
          })}
        </Routes>
      </Suspense>
    </div>
  );
};

export const MainLayout = observer(MainLayoutComp);
