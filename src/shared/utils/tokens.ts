import { jwtDecode } from 'jwt-decode';

export type DecodedAccessToken = {
  login: string;
};

export const getDecodedAccessToken = (token: string | null): DecodedAccessToken | null => {
  return token ? jwtDecode(token) : null;
};

export const isAuth = () => {
  return Boolean(localStorage.getItem('accessToken')?.length);
};
