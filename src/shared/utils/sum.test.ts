import { describe, expect, test } from "@jest/globals";
import { sum } from "./sum";

describe("Common", () => {
  test("sum", () => {
    expect(sum(1, 1)).toBe(2);
  });
});
