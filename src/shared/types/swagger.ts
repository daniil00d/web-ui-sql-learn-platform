/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

/** CourseCreateDto */
export interface CourseCreateDto {
  /**
   * Title
   * @minLength 3
   * @maxLength 50
   */
  title: string;
  /**
   * Topic
   * @minLength 3
   * @maxLength 50
   */
  topic: string;
  /**
   * Description
   * @minLength 3
   * @maxLength 50
   */
  description: string;
}

/** CourseCreateResponse */
export interface CourseCreateResponse {
  /** Course Id */
  course_id: number;
}

/** CourseDto */
export interface CourseDto {
  /**
   * Title
   * @minLength 3
   * @maxLength 50
   */
  title: string;
  /**
   * Topic
   * @minLength 3
   * @maxLength 50
   */
  topic: string;
  /**
   * Description
   * @minLength 3
   * @maxLength 50
   */
  description: string;
  /** Course Id */
  course_id: number;
}

/** HTTPValidationError */
export interface HTTPValidationError {
  /** Detail */
  detail?: ValidationError[];
}

/** LessonCreateDto */
export interface LessonCreateDto {
  /**
   * Title
   * @minLength 3
   */
  title: string;
  /**
   * Topic
   * @minLength 3
   */
  topic: string;
  /**
   * Material
   * @minLength 3
   */
  material: string;
  /** Course Id */
  course_id: number;
}

/** LessonCreateResponse */
export interface LessonCreateResponse {
  /** Lesson Id */
  lesson_id: number;
}

/** LessonDto */
export interface LessonDto {
  /**
   * Title
   * @minLength 3
   */
  title: string;
  /**
   * Topic
   * @minLength 3
   */
  topic: string;
  /**
   * Material
   * @minLength 3
   */
  material: string;
  /** Course Id */
  course_id: number;
  /** Lesson Id */
  lesson_id: number;
}

/** TaskCreateDto */
export interface TaskCreateDto {
  /**
   * Title
   * @minLength 3
   */
  title: string;
  /**
   * Formulation
   * @minLength 3
   */
  formulation: string;
  /** Answer */
  answer: string;
  /**
   * Complexity
   * @minLength 3
   */
  complexity: string;
  /**
   * Topic
   * @minLength 3
   */
  topic: string;
  /**
   * Operator Array
   * @minLength 3
   */
  operator_array: string;
  /** Lesson Id */
  lesson_id: number;
}

/** TaskCreateResponse */
export interface TaskCreateResponse {
  /** Task Id */
  task_id: number;
}

/** TaskDto */
export interface TaskDto {
  /**
   * Title
   * @minLength 3
   */
  title: string;
  /**
   * Formulation
   * @minLength 3
   */
  formulation: string;
  /** Answer */
  answer: string;
  /**
   * Complexity
   * @minLength 3
   */
  complexity: string;
  /**
   * Topic
   * @minLength 3
   */
  topic: string;
  /**
   * Operator Array
   * @minLength 3
   */
  operator_array: string;
  /** Lesson Id */
  lesson_id: number;
  /** Task Id */
  task_id: number;
}

/** User */
export interface User {
  /** Login */
  login: string;
  /** Password */
  password: string;
}

/** UserCreateDto */
export interface UserCreateDto {
  /**
   * Login
   * @minLength 3
   * @maxLength 50
   */
  login: string;
  /**
   * Email
   * @minLength 3
   * @maxLength 50
   */
  email: string;
  /**
   * Password
   * @minLength 3
   * @maxLength 50
   */
  password: string;
}

/** UserCreateResponse */
export interface UserCreateResponse {
  /** User Id */
  user_id: number;
}

/** UserDto */
export interface UserDto {
  /** User Id */
  user_id: number;
  /** Login */
  login: string;
  /** Email */
  email: string;
}

/** UserUpdateDto */
export interface UserUpdateDto {
  /** Email */
  email: string;
}

/** UserUpdateResponse */
export interface UserUpdateResponse {
  /** User Id */
  user_id: number;
  /** Login */
  login: string;
  /** Email */
  email: string;
}

/** ValidationError */
export interface ValidationError {
  /** Location */
  loc: any[];
  /** Message */
  msg: string;
  /** Error Type */
  type: string;
}
