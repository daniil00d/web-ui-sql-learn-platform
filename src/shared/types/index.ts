import React from 'react';

export type Route = {
  path: string;
  exact?: boolean;
  private?: boolean;
  element: React.ReactNode;
  redirect?: string;
};
