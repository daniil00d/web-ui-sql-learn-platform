import { Typography } from '@mui/material';
import React from 'react';
import Markdown from 'react-markdown';

export const CustomMarkdown = ({ text }: { text: string | null | undefined }) => {
  return (
    <Markdown
      components={{
        h1(props) {
          const { children: ch } = props;
          return (
            <Typography variant='h4' sx={{ margin: '16px 0' }}>
              {ch}
            </Typography>
          );
        },
        h2(props) {
          const { children: ch } = props;
          return (
            <Typography variant='h5' sx={{ margin: '12px 0' }}>
              {ch}
            </Typography>
          );
        },
        p(props) {
          const { children: ch } = props;
          return (
            <Typography variant='body1' sx={{ margin: '12px 0' }}>
              {ch}
            </Typography>
          );
        },
      }}
    >
      {text}
    </Markdown>
  );
};
