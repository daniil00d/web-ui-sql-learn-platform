import React from 'react';

import { Navigate, Route } from 'react-router-dom';
import { AUTH_ROUTE } from '@/app/constants/routes';
import { authStore } from '@/app/state';

export const authRedirect = (component: React.ReactNode) => {
  if (authStore.isAuth) return component;

  return <Route path='*' element={<Navigate to={AUTH_ROUTE} replace />} />;
};
