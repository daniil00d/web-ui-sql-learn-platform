import ReactDOM from 'react-dom/client';
import { MainLayout } from './shared/layouts';
import { BrowserRouter } from 'react-router-dom';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ToastContainer } from 'react-toastify';

// import '@/shared/styles/reset.css';
import 'react-toastify/dist/ReactToastify.css';

const defaultTheme = createTheme();
const queryClient = new QueryClient();

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
  <BrowserRouter>
    <QueryClientProvider client={queryClient}>
      <ThemeProvider theme={defaultTheme}>
        <MainLayout />
        <ToastContainer position='bottom-right' />
      </ThemeProvider>
    </QueryClientProvider>
  </BrowserRouter>,
);
