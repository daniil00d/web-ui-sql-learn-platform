import { useMutation } from 'react-query';
import { useNavigate } from 'react-router';
import { toast } from 'react-toastify';

import { RegisterForm } from './form';
import { apiClient } from '@/app/api';
import { AUTH_ROUTE } from '@/app/constants/routes';

const RegisterPage = () => {
  const navigate = useNavigate();
  const { mutate, isLoading } = useMutation<
    unknown,
    unknown,
    { login: string; password: string; email: string }
  >((data) => {
    return apiClient
      .post<{ access_token: string }>('/users', data)
      .then((data) => {
        toast.success('Вы успешно зарегистрировались!', { autoClose: 1000 });

        navigate(AUTH_ROUTE);
      })
      .catch((err) => {
        toast.error(`Error: ${err.toString()}`, { autoClose: 1000 });
      });
  });

  const handleRegister = ({
    login,
    password,
    email,
  }: {
    login: string;
    password: string;
    email: string;
  }) => {
    mutate({ login, password, email });
  };

  return <RegisterForm onRegister={handleRegister} isLoading={isLoading} />;
};

export default RegisterPage;
