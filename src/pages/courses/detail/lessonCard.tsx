import React from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.module.scss';

interface LessonCardProps {
  title: string;
  topic: string;
  id: number;
}

export const LessonCard = (props: LessonCardProps) => {
  return (
    <Link className={styles.lessonCard} to={`/lessons/${props.id}`}>
      <div className={styles.title}>
        <h3>{props.title}</h3>
      </div>
      <span className={styles.topic}>{props.topic}</span>
    </Link>
  );
};
