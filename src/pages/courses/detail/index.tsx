import React, { useEffect, useState } from 'react';
import { apiClient } from '@/app/api';
import { Loader } from '@/shared/components';
import { CourseDto, LessonDto } from '@/shared/types/swagger';
import { useParams } from 'react-router';
import { Sidebar } from './sidebar';
import { Box, Typography } from '@mui/material';
import styles from './styles.module.scss';
import { LessonCard } from './lessonCard';

const CourseDetail = () => {
  const { id } = useParams<{ id: string }>();
  const [loading, setLoading] = useState(false);
  const [course, setCourse] = useState<CourseDto | null>(null);
  const [lessons, setLessons] = useState<LessonDto[]>([]);

  useEffect(() => {
    setLoading(true);
    const getCourse = apiClient.get<CourseDto>(`/courses/${id}`).then((response) => {
      setCourse(response.data);
    });

    const getLessons = apiClient
      .get<LessonDto[]>(`/lessons/list/?course_ids=${id}`)
      .then((response) => {
        setLessons(response.data);
      });

    Promise.all([getCourse, getLessons]).finally(() => setLoading(false));
  }, [id]);

  if (loading) {
    return <Loader />;
  }

  return (
    <Box component='main' className={styles.main}>
      <Sidebar id={Number(id)} />
      <Box component='div' className={styles.cnt}>
        <div className={styles.header}>
          <Typography component='h2' className={styles.title} variant='h4'>
            Мини-курс. {course?.title}
          </Typography>
          <Typography variant='subtitle1' className={styles.description}>
            Описание. {course?.description}
          </Typography>
        </div>
        <Typography className={styles.lessonTitle} variant='h5'>
          Связанные уроки:
        </Typography>
        <div className={styles.lessons}>
          {lessons.length ? (
            lessons.map((lesson) => {
              return <LessonCard title={lesson.title} topic={lesson.topic} id={lesson.lesson_id} />;
            })
          ) : (
            <Typography variant='subtitle1'>
              Для данного мини-курса уроков пока не добавлено.
            </Typography>
          )}
        </div>
        <ul></ul>
      </Box>
    </Box>
  );
};

export default CourseDetail;
