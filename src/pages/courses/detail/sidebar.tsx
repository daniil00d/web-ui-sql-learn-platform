import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import { CourseDto } from '@/shared/types/swagger';
import { apiClient } from '@/app/api';
import { Link } from 'react-router-dom';

const drawerWidth = 240;

export const Sidebar = ({ id }: { id: number }) => {
  const [courses, setCourses] = useState<CourseDto[]>([]);

  useEffect(() => {
    apiClient.get<CourseDto[]>('/courses/list/').then((response) => {
      setCourses(response.data);
    });
  }, [id]);

  return (
    <Drawer
      variant='permanent'
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        [`& .MuiDrawer-paper`]: { width: drawerWidth, boxSizing: 'border-box', position: 'sticky' },
      }}
    >
      <CssBaseline />
      <Box sx={{ overflow: 'auto' }}>
        <List>
          {courses.map((course, index) => (
            <ListItem
              key={course.course_id}
              disablePadding
              style={{ background: id === course.course_id ? '#e5e5e5' : 'inherit' }}
            >
              <Link
                to={`/courses/${course.course_id}`}
                style={{ width: '100%', textDecoration: 'none', color: 'black' }}
              >
                <ListItemButton>
                  <ListItemText primary={course.title} />
                </ListItemButton>
              </Link>
            </ListItem>
          ))}
        </List>
      </Box>
    </Drawer>
  );
};
