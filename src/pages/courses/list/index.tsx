import React, { useEffect, useState } from 'react';
import { apiClient } from '@/app/api';
import { Loader } from '@/shared/components';
import { useDebounce } from '@/shared/hooks';
import { CourseDto } from '@/shared/types/swagger';
import { Container, TextField, Typography } from '@mui/material';
import styles from './styles.module.scss';
import { CourseCard } from './courseCard';

const CourseList = () => {
  const [loading, setLoading] = useState(false);
  const [courses, setCourses] = useState<CourseDto[]>([]);
  const [search, setSearch] = useState('');

  const debouncedSearch = useDebounce(search);

  useEffect(() => {
    setLoading(true);
    apiClient
      .get<CourseDto[]>('/courses/list/', { params: { search: debouncedSearch } })
      .then((response) => {
        setCourses(response.data || []);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [debouncedSearch]);

  return (
    <Container maxWidth='xl'>
      <div className={styles.header}>
        <Typography variant='h4'>Мини-курсы</Typography>
        <Typography variant='subtitle1'>Здесь вы можете выбрать мини-курс по душе</Typography>
      </div>
      <TextField
        margin='normal'
        size='medium'
        fullWidth
        label='Найти по названию'
        onChange={(e) => setSearch(e.target.value)}
        autoFocus
      />
      <div className={styles.list}>
        {loading ? <Loader /> : null}
        {!loading
          ? courses.map((course) => {
              return <CourseCard title={course.title} id={course.course_id} topic={course.topic} />;
            })
          : null}
      </div>
    </Container>
  );
};

export default CourseList;
