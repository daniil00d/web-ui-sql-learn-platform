import React from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.module.scss';
import { Typography } from '@mui/material';

interface CourseCardProps {
  title: string;
  id: number;
  topic: string;
}

export const CourseCard = (props: CourseCardProps) => {
  return (
    <Link to={`/courses/${props.id}`} className={styles.courseCard}>
      <Typography variant='body1'>{props.title}</Typography>
      <div className={styles.courseCardTopic}>
        <Typography variant='body2'>{props.topic}</Typography>
      </div>
    </Link>
  );
};
