import { apiClient } from '@/app/api';
import { CustomMarkdown, Loader } from '@/shared/components';
import { LessonDto, TaskDto } from '@/shared/types/swagger';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { Container, Typography } from '@mui/material';
import BasicRating from './rating';
import { LessonBreadcrumbs } from './breadcrumbs';

const LessonDetail = () => {
  const { id } = useParams<{ id: string }>();
  const [loading, setLoading] = useState(false);
  const [lesson, setLesson] = useState<LessonDto | null>(null);
  const [tasks, setTasks] = useState<TaskDto[]>([]);

  useEffect(() => {
    setLoading(true);
    const getLesson = apiClient.get<LessonDto>(`/lessons/${id}`).then((response) => {
      setLesson(response.data);
    });

    const getTasks = apiClient.get<TaskDto[]>(`/tasks/list/?lesson_ids=${id}`).then((response) => {
      setTasks(response.data);
    });

    Promise.all([getLesson, getTasks]).finally(() => setLoading(false));
  }, [id]);

  if (loading) {
    return <Loader />;
  }

  return (
    <Container maxWidth='xl'>
      <LessonBreadcrumbs title={lesson?.title} courseId={lesson?.course_id} />
      <div className='header'>
        <Typography variant='h4' sx={{ margin: '16px 0', fontWeight: 'bold' }}>
          Урок. {lesson?.title}
        </Typography>
      </div>
      <CustomMarkdown text={lesson?.material} />
      <div className='rating'>
        <BasicRating />
      </div>

      <ul>
        {tasks.map((task) => {
          return (
            <li>
              <Link to={`/tasks/${task.task_id}`}>{task.title}</Link>
            </li>
          );
        })}
      </ul>
    </Container>
  );
};

export default LessonDetail;
