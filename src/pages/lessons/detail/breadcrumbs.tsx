import React, { useEffect, useState } from 'react';
import Typography from '@mui/material/Typography';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import { Link as BrowserLink } from 'react-router-dom';
import { apiClient } from '@/app/api';
import { CourseDto } from '@/shared/types/swagger';

function handleClick(event: React.MouseEvent<HTMLDivElement, MouseEvent>) {
  event.preventDefault();
  console.info('You clicked a breadcrumb.');
}

interface LessonBreadcrumbsProps {
  title: string | undefined;
  courseId: number | undefined;
}

export function LessonBreadcrumbs({ title, courseId }: LessonBreadcrumbsProps) {
  const [courseTitle, setCourseTitle] = useState('');

  useEffect(() => {
    courseId &&
      apiClient.get<CourseDto>(`/courses/${courseId}`).then((response) => {
        setCourseTitle(response.data.title);
      });
  }, [courseId]);

  return (
    <div role='presentation' onClick={handleClick} style={{ margin: '16px 0 32px' }}>
      <Breadcrumbs aria-label='breadcrumb'>
        <Link underline='hover' color='inherit' to='/courses' component={BrowserLink}>
          Мини-курсы
        </Link>
        <Link underline='hover' color='inherit' to={`/courses/${courseId}`} component={BrowserLink}>
          {courseTitle || 'Loading...'}
        </Link>
        <Typography color='text.primary'>{title}</Typography>
      </Breadcrumbs>
    </div>
  );
}
