import * as React from 'react';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';

export default function BasicRating() {
  const [value, setValue] = React.useState<number | null>(4);

  return (
    <Box
      sx={{
        '& > legend': { mt: 2 },
        display: 'flex',
        gap: '8px',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <Typography component='legend'>Оцените урок:</Typography>
      <Rating
        name='simple-controlled'
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      />
    </Box>
  );
}
