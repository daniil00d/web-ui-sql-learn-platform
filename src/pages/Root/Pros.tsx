import { Typography } from '@mui/material';

import styles from './styles.module.scss';

export const Pros = ({ title, desc }: { title: string; desc: string }) => {
  return (
    <div className={styles.item}>
      <Typography variant='h6' sx={{ mb: 2 }}>
        {title}
      </Typography>
      <Typography variant='subtitle1' color='#6a6a6a'>
        {desc}
      </Typography>
    </div>
  );
};
