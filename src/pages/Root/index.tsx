import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Img from './img.svg';
import { Pros } from './Pros';
import styles from './styles.module.scss';

function Copyright() {
  return (
    <Typography variant='body2' color='text.secondary' align='center'>
      {'Copyright © '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const pros = [
  {
    title: '🤌 Удобство изучения',
    desc: 'Наша платформа предлагает удобную и гибкую среду для изучения SQL. Вы можете получить доступ к обучающему материалу в любое время и из любого места, совершенствуя свои навыки на практике.',
  },
  {
    title: '😍 Интерактивность',
    desc: 'Мы предлагаем интерактивные задачи и упражнения, которые позволяют вам непосредственно применять свои знания SQL и сразу же видеть результаты. Практика через взаимодействие усиливает вашу обучение и уверенность в навыках.',
  },
  {
    title: '⭐️ Направленные курсы',
    desc: 'Наша платформа включает серию курсов, специально разработанных для изучения разных аспектов SQL. Это позволяет вам выбирать и проходить только необходимый и интересующий вас материал, экономя время и получая максимум полезной информации.',
  },
  {
    title: '🧩 Поддержка и сообщество',
    desc: 'Мы предоставляем поддержку вам в процессе обучения. Вы сможете общаться с другими учащимися и экспертами, задавать вопросы, делиться опытом и получать ценные советы. Доступ к сообществу помогает вам расширить свои знания и установить полезные связи.',
  },
];

// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme();

export default function Root() {
  return (
    <ThemeProvider theme={defaultTheme}>
      <CssBaseline />
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: '#155bd5',
            pt: 15,
            pb: 20,
            height: 'calc(100vh - 65px)',
          }}
        >
          <Container
            maxWidth='xl'
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              height: '100%',
            }}
          >
            <div className='lef'>
              <Typography component='h1' variant='h2' align='left' color='white' gutterBottom>
                SQL Platform
              </Typography>
              <Typography
                variant='h6'
                align='left'
                color='rgb(220, 220, 220)'
                paragraph
                maxWidth={600}
              >
                Добро пожаловать на платформу обучения SQL! Мы предоставляем удобный и интерактивный
                способ изучения SQL языка. Независимо от вашего опыта мы поможем вам начать!
              </Typography>
              <Stack sx={{ pt: 4 }} direction='row' spacing={2} justifyContent='left'>
                <Button variant='contained' color='inherit'>
                  Начать!
                </Button>
              </Stack>
            </div>
            <div className='right'>
              <img src={Img} alt='img' width={700} />
            </div>
          </Container>
        </Box>
      </main>
      <section>
        <Container maxWidth='xl'>
          <Typography variant='h5' align='center' gutterBottom sx={{ mt: 1, mb: 5 }}>
            🔫 Наши преимущества 🔫
          </Typography>
          <div className={styles.pros}>
            {pros.map((pro) => (
              <Pros {...pro} />
            ))}
          </div>
        </Container>
      </section>
      {/* Footer */}
      <Box sx={{ bgcolor: 'background.paper', p: 6 }} component='footer'>
        <Typography variant='subtitle1' align='center' color='text.secondary' component='p'>
          Всегда ваша ❤️ команда - SQL Platform Team
        </Typography>
        <Copyright />
      </Box>
      {/* End footer */}
    </ThemeProvider>
  );
}
