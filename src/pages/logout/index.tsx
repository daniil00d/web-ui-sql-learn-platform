import { authStore } from '@/app/state';

const Logout = () => {
  authStore.logout();

  return <>Logout...</>;
};

export default Logout;
