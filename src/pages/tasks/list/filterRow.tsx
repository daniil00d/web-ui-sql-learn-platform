import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
} from '@mui/material';
import styles from './styles.module.scss';

interface FilterRowProps {
  search: string;
  operator: string;
  complexity: string;

  onSearchChange: (value: string) => void;
  onOperatorChange: (value: string) => void;
  onComplexityChange: (value: string) => void;
}

export const FilterRow = (props: FilterRowProps) => {
  const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.onSearchChange(event.target.value);
  };

  const onOperatorChange = (event: SelectChangeEvent<unknown>) => {
    props.onOperatorChange(event.target.value as string);
  };

  const onComplexityChange = (event: SelectChangeEvent<unknown>) => {
    props.onComplexityChange(event.target.value as string);
  };

  return (
    <div className={styles.filterRow}>
      <div className={styles.search}>
        <TextField
          fullWidth
          id='outlined-basic'
          label='Поиск по названию'
          variant='outlined'
          value={props.search}
          onChange={onSearchChange}
        />
      </div>
      <div className={styles.tags}>
        <FormControl fullWidth>
          <InputLabel id='operators'>Операторы</InputLabel>
          <Select
            labelId='operators'
            label='Операторы'
            onChange={onOperatorChange}
            value={props.operator}
          >
            <MenuItem value='select'>select</MenuItem>
            <MenuItem value='insert'>insert</MenuItem>
            <MenuItem value='orderBy'>orderBy</MenuItem>
          </Select>
        </FormControl>
      </div>
      <div className={styles.complexity}>
        <FormControl fullWidth>
          <InputLabel id='complexity'>Сложность</InputLabel>
          <Select
            labelId='complexity'
            label='Операторы'
            onChange={onComplexityChange}
            value={props.complexity}
          >
            <MenuItem value='high'>сложно</MenuItem>
            <MenuItem value='medium'>средняя</MenuItem>
            <MenuItem value='low'>легко</MenuItem>
          </Select>
        </FormControl>
      </div>
    </div>
  );
};
