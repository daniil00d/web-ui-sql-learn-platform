import { Button, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

import styles from './styles.module.scss';
import { useMemo } from 'react';

interface TaskItemProps {
  title: string;
  operators: string[];
  id: number;
}

export const TaskItem = (props: TaskItemProps) => {
  const count = useMemo(() => Math.round(Math.random() * 10), []);

  return (
    <div className={styles.taskItem}>
      <div className='left'>
        <Typography>{props.title}</Typography>
        <div className={styles.row}>
          <Typography variant='body2' sx={{ color: 'text.secondary' }}>
            Решивших: {count}
          </Typography>
          <div className={styles.operators}>
            {props.operators.map((operator) => (
              <div className={styles.badge}>{operator}</div>
            ))}
          </div>
        </div>
      </div>
      <div className='right'>
        <Link to={`/tasks/${props.id}`}>
          <Button variant='outlined'>Решать</Button>
        </Link>
      </div>
    </div>
  );
};
