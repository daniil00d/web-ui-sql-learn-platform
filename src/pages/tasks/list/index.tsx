import React, { useEffect, useState } from 'react';
import { Container, Typography } from '@mui/material';

import styles from './styles.module.scss';
import { TaskDto } from '@/shared/types/swagger';
import { apiClient } from '@/app/api';
import { TaskItem } from './taskItem';
import { FilterRow } from './filterRow';
import { useDebounce } from '@/shared/hooks';

const TaskList = () => {
  const [loading, setLoading] = useState(false);
  const [tasks, setTasks] = useState<TaskDto[]>([]);

  // filters
  const [search, setSearch] = useState('');
  const [operator, setOperator] = useState('');
  const [complexity, setComplexity] = useState('');

  const debouncedSearch = useDebounce(search, 500);

  useEffect(() => {
    setLoading(true);
    apiClient
      .get('/tasks/list/', { params: { search: debouncedSearch || undefined } })
      .then((response) => {
        setTasks(response.data);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [debouncedSearch]);

  return (
    <Container maxWidth='xl' className={styles.container}>
      <div className={styles.header}>
        <Typography variant='h4' sx={{ mt: 3 }}>
          Каталог задач
        </Typography>
        <Typography variant='subtitle1'>
          Это каталог задач, здесь вы можете найти задачи по душе и решать их, увеличивая свой
          рейтинг
        </Typography>
      </div>
      <div className={styles.filter}>
        <FilterRow
          search={search}
          onSearchChange={(value) => setSearch(value)}
          operator={operator}
          complexity={complexity}
          onOperatorChange={(value) => setOperator(value)}
          onComplexityChange={(value) => setComplexity(value)}
        />
      </div>
      <div className={styles.list}>
        {loading
          ? 'loading...'
          : tasks.map((task) => {
              return (
                <TaskItem
                  id={task.task_id}
                  title={task.title}
                  operators={task.operator_array.split(',').map((operator) => operator.trim())}
                />
              );
            })}
      </div>
    </Container>
  );
};

export default TaskList;
