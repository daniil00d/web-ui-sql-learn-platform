import React, { useEffect, useState } from 'react';
import Editor from '@monaco-editor/react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Button } from '@mui/material';
import { TaskDto } from '@/shared/types/swagger';
import { useParams } from 'react-router';
import { apiClient } from '@/app/api';
import { CustomMarkdown, Loader } from '@/shared/components';
import styles from './styles.module.scss';

const SQL_EXP = `
with ships_outcomes as (
  select o.ship, c.country, c.bore from outcomes as o
  join classes as c on o.ship = c.class
  union
  select s.name, c.country, c.bore from classes as c
  join ships as s on c.class = s.class)
  
  select so.country, round(cast(avg(POW(so.bore,3)/2.0) as numeric), 2) as half_cube_bore_avg from ships_outcomes as so
  group by so.country
`;

const commonCond = `
Рассматривается БД кораблей, участвовавших во второй мировой войне. Имеются
следующие отношения: 
- Classes (class, type, country, numGuns, bore, displacement)
- Ships (name, class, launched) - Battles (name, date) 
- Outcomes (ship, battle, result) 

Корабли в «классах» построены по одному и тому же
проекту, и классу присваивается либо имя первого корабля, построенного по
данному проекту, либо названию класса дается имя проекта, которое не совпадает
ни с одним из кораблей в БД. Корабль, давший название классу, называется
головным. 

Отношение Classes содержит имя класса, тип (bb для боевого (линейного)
корабля или bc для боевого крейсера), страну, в которой построен корабль, число
главных орудий, калибр орудий (диаметр ствола орудия в дюймах) и водоизмещение (
вес в тоннах). В отношении Ships записаны название корабля, имя его класса и год
спуска на воду. 

В отношение Battles включены название и дата битвы, в которой
участвовали корабли, а в отношении Outcomes – результат участия данного корабля
в битве (потоплен-sunk, поврежден - damaged или невредим - OK). Замечания. 1) В
отношение Outcomes могут входить корабли, отсутствующие в отношении Ships. 2)
Потопленный корабль в последующих битвах участия не принимает.
`;

const TaskDetail = () => {
  const { id } = useParams<{ id: string }>();
  const [task, setTask] = useState<TaskDto | null>(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    apiClient
      .get<TaskDto>(`/tasks/${id}`)
      .then((response) => {
        setTask(response.data);
      })
      .finally(() => setLoading(false));
  }, [id]);

  return (
    <div className={styles.container}>
      <div className={styles.formulation}>
        {loading ? (
          <Loader />
        ) : (
          <>
            <div className={styles.formulationHeader}>
              <Typography variant='h4'>{task?.title}</Typography>
              <Typography variant='subtitle1'>Сложность: {task?.complexity}</Typography>
            </div>
            <Accordion>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls='panel1a-content'
                id='panel1a-header'
              >
                <Typography>Схема базы данных</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <CustomMarkdown text={commonCond} />
                <img src='https://www.sql-ex.ru/images/ships.gif' alt='cond' />
              </AccordionDetails>
            </Accordion>

            <div className={styles.content}>
              <Typography variant='h6'>Условие</Typography>
              <CustomMarkdown text={task?.formulation} />
            </div>
          </>
        )}
      </div>
      <div className={styles.editor}>
        <Editor defaultLanguage='sql' defaultValue={SQL_EXP} />
        <div className={styles.bottomPanel}>
          <Button variant='contained' color='success' style={{ height: 40 }}>
            Запустить
          </Button>
          <Button variant='contained' color='primary' style={{ height: 40 }}>
            Отправить решение
          </Button>
        </div>
      </div>
    </div>
  );
};

export default TaskDetail;
