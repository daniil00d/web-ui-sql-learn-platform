import { useMutation } from 'react-query';
import { useNavigate } from 'react-router';
import { toast } from 'react-toastify';

import { AuthForm } from './form';
import { apiClient } from '@/app/api';
import { authStore } from '@/app/state';

const AuthPage = () => {
  const navigate = useNavigate();
  const { mutate, isLoading } = useMutation<unknown, unknown, { login: string; password: string }>(
    (data) => {
      return apiClient
        .post<{ access_token: string }>('/auth/login', data)
        .then((data) => {
          const token = data.data.access_token;

          localStorage.setItem('accessToken', token);
          toast.success('Вы успешно авторизовались!', { autoClose: 1000 });

          authStore.login(token);
          navigate('/');
        })
        .catch((err) => {
          toast.error(`Error: ${err.toString()}`, { autoClose: 1000 });
        });
    },
  );

  const handleLogin = ({ login, password }: { login: string; password: string }) => {
    mutate({ login, password });
  };

  return <AuthForm onLogin={handleLogin} isLoading={isLoading} />;
};

export default AuthPage;
