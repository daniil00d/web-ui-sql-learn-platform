import React, { useEffect, useState } from 'react';
import { API_URL } from '@/app/constants';

interface User {
  login: string;
  email: string;
}

const Root = () => {
  const [users, setUsers] = useState<User[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`${API_URL}/users/list/`)
      .then((response) => response.json())
      .then((data) => setUsers(data))
      .finally(() => setLoading(false));
  }, []);

  return (
    <ul>
      {!loading
        ? users.map((user) => (
            <li key={user.login}>
              login: {user.login}; email: {user.email}{' '}
            </li>
          ))
        : 'loading'}
    </ul>
  );
};

export default Root;
