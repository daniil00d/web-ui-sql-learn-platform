PATH_DIR="src/shared/types"

curl "http://localhost:8002/openapi.json" > spec.json

npx swagger-typescript-api -p spec.json -o $PATH_DIR -n swagger.ts --no-client

rm spec.json